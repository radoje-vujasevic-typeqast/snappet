import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import "@coreui/coreui-pro/dist/css/coreui.min.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
