import { CCard, CCardBody, CCardHeader, CCol } from "@coreui/react-pro";
import { useNavigate } from "react-router-dom";

function Home({ data }) {
  const userIds = [...new Set(data.map((item) => item.UserId))].sort();
  const navigate = useNavigate();

  return (
    <div style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
      {userIds.map((item) => (
        <CCol
          key={item}
          lg={3}
          style={{ padding: "15px 5px 0 5px", cursor: "pointer" }}
          onClick={() => navigate(`/user/${item}`)}
        >
          <CCard className={`border-primary`} textColor="primary">
            <CCardHeader>Go to user:</CCardHeader>
            <CCardBody>{item}</CCardBody>
          </CCard>
        </CCol>
      ))}
    </div>
  );
}

export default Home;
