import { CSmartTable } from "@coreui/react-pro";
import { useParams } from "react-router-dom";

function Table({ data }) {
  const columns = [
    { key: "UserId" },
    { key: "ExerciseId" },
    { key: "SubmittedAnswerId", label: "Answer ID" },
    { key: "Subject" },
    { key: "Domain" },
    { key: "LearningObjective" },
    { key: "Correct" },
    { key: "Progress" },
    { key: "Difficulty" },
  ];
  const { id } = useParams();
  const dataForUserId = data
    .filter((item) => item.UserId === Number(id))
    .sort(({ ExerciseId: id1 }, { ExerciseId: id2 }) => (id1 > id2 ? 1 : -1));

  return (
    <CSmartTable
      columnSorter
      pagination={false}
      itemsPerPage={dataForUserId.length}
      tableProps={{ hover: true }}
      items={dataForUserId}
      columns={columns}
    />
  );
}

export default Table;
