import { CSidebar, CHeader, CSidebarHeader } from "@coreui/react-pro";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Table from "./Table";
import Home from "./Home";
import data from "./work.json";

function App() {
  const today = new Date("2015-03-24T11:30:00");
  const todayMidnight = new Date("2015-03-24T00:00:00");
  const todayData = data.filter(
    (item) =>
      new Date(item.SubmitDateTime) <= today &&
      new Date(item.SubmitDateTime) >= todayMidnight
  );

  return (
    <BrowserRouter>
      <div style={{ display: "flex" }}>
        <CSidebar hidden={false}>
          <CSidebarHeader>Snappet</CSidebarHeader>
        </CSidebar>
        <div style={{ width: "100%", minHeight: "100vh" }}>
          <CHeader />
          <div className="px-3">
            <Routes>
              <Route path="/user/:id" element={<Table data={todayData} />} />
              <Route path="/" element={<Home data={todayData} />} />
            </Routes>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
